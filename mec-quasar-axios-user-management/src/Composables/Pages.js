import { ref } from "vue";
import axios from "axios";
import { rows } from "../pages/ListOfUsers";

let ListOfUsers = ref([]);
let user = ref([]);
let selectedRow = ref(null);
let isUpdating = ref(false);
let id = ref(null);

let form = ref({
    name: null,
    username: null,
    email: null,
    street: null,
    suite: null,
    city: null,
    zipcode: null,
    number: null,
    website: null,
    company: null,
    catchphrase: null,
    bs: null,
});

export { ListOfUsers, user, selectedRow, isUpdating, form, id };
